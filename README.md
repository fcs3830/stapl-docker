# STAPL Docker

This repository stores Docker images for the [STAPL](http://parasol-lab.gitlab.io/stapl-home) project. STAPL is a library, requiring only a C++ compiler, the Boost libraries, and established communication libraries such as MPI.

Compilers supported are:
- GCC 4.8.2 - 6.3.0
  - 4.8 ([Dockerfile](https://gitlab.com/parasol-lab/tooling/stapl-docker/blob/master/gcc/4.8/Dockerfile))
  - 4.9 ([Dockerfile](https://gitlab.com/parasol-lab/tooling/stapl-docker/blob/master/gcc/4.9/Dockerfile))
  - 5.4 ([Dockerfile](https://gitlab.com/parasol-lab/tooling/stapl-docker/blob/master/gcc/5.4/Dockerfile))
- Clang 3.6 - 3.7 (using libstdc++ for STL)
- Intel C++ 17 (using gcc compatability)

Tags for images follow the format `parasol/stapl-build:compiler_x.x-boost_x.xx-mpi_x.x`.  For example, GCC 6.3 + Boost 1.63 + MPICH 3.2 would have the tag:

```
parasol/stapl-build:gcc_6.3-boost_1.63-mpich_3.2
```
